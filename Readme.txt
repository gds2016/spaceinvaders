

You can use your previous projects or other internet resources to
complete the quiz, but the majority of the code has to be your own
work (i.e. Don't submit somebody else's Unity project as your own.)

Create a simplified Space Invaders game
https://en.wikipedia.org/wiki/Space_Invaders from scratch. Start by
forking the repository https://bitbucket.org/yusufpisan/spaceinvaders

Name your forked repository with your initials (something like
spaceinvaders-yp) and make it readable by 'yusufpisan'

Create a minimal game that will get you the maximum marks. Do not
implement unnecessary features. Make your own assumptions as needed if
it is not specified below.

Fill out the self assessment below and create a pull request to submit your work.

===========================================

Name:  --->  Your Name  <----

Replace ? with your expected marks.


  ?   / 1   A rectangle representing the SHIP is displayed at the bottom of the screen and 3 squares representing the INVADERS is displayed on the top of the screen. SHIP moves using keys A and D
  
  ?   / 1   Score of 0 is displayed on the screen.

  ?   / 2   INVADERS move left to right on the screen, but do not exit the screen.

  ?   / 2   When “spacebar” is pressed, the SHIP fires a single missile towards the top of the screen.

  ?   / 2   If MISSILE collides with an INVADER, score increases, MISSILE disappears and INVADER disappears.

  ?   / 2   Pressing “R”, resets the score and restarts the game.



  ?   / 10  Total 

===========================================



